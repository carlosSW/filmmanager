package cl.videoclub.managed.bean;

/**
 * @author fabricio
 */
import java.util.Calendar;
import java.util.GregorianCalendar;

public abstract class Utileria {
	public static String getFechaActual() {

		Calendar fecha = new GregorianCalendar();
		int año = fecha.get(Calendar.YEAR);
		int mes = fecha.get(Calendar.MONTH);
		int dia = fecha.get(Calendar.DAY_OF_MONTH);
		String date = "";
		date = "" + dia + "-" + (mes + 1) + "-" + año;
		return date;
	}
}
