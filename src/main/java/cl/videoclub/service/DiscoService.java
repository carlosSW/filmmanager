package cl.videoclub.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import cl.videoclub.model.Disco;
import cl.videoclub.dao.IDiscoDAO;
/**
 * 
 * @author Carlos M. Toledo
 *
 */
@Transactional(readOnly=true)
public class DiscoService implements IDiscoService{
	
	IDiscoDAO discoDAO;

	public IDiscoDAO getDiscoDAO() {
		return discoDAO;
	}

	public void setDiscoDAO(IDiscoDAO discoDAO) {
		this.discoDAO = discoDAO;
	}

	@Transactional(readOnly=false)
	public void add(Object disco) {
		getDiscoDAO().add(disco);
		
	}

	@Transactional(readOnly=false)
	public void update(Object disco) {
		getDiscoDAO().update(disco);
		
	}

	@Transactional(readOnly=false)
	public void delete(Object disco) {
		getDiscoDAO().delete(disco);
		
	}

	public Disco getById(Object id) {
		return (Disco) getDiscoDAO().getById(id);
	}

	public List<Disco> getAll() {
		return getDiscoDAO().getAll();
	}

	
}
