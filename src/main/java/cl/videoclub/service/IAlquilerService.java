package cl.videoclub.service;

import java.util.List;


import cl.videoclub.model.Alquiler;
/**
 * 
 * @author Carlos M. Toledo
 *
 */
public interface IAlquilerService extends IService{
		
	public List<Alquiler> getAlquilerListBySocioId(int socioId);
	public Alquiler getAlquilerListByDiscoId(int discoId);
	public Alquiler getAlquilerByIdDiscoAndIdSocio(int idDisco, int IdSocio);
}
