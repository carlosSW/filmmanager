package cl.videoclub.managed.bean;

/**
 * @author Carlos M. Toledo
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.PostLoad;
import javax.persistence.PostUpdate;
import javax.persistence.PreUpdate;

import org.springframework.dao.DataAccessException;

import cl.videoclub.model.Alquiler;
import cl.videoclub.model.Disco;
import cl.videoclub.service.IAlquilerService;
import cl.videoclub.service.IDiscoService;

@ManagedBean(name = "alquilerMB")
@RequestScoped
public class AlquilerManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String SUCCESS = "success";
	private static final String ERROR = "error";

	/**
	 * servicio para ejecutar transacciones con Alquiler la propiedad es
	 * inyectada
	 */
	@ManagedProperty(value = "#{AlquilerService}")
	IAlquilerService alquilerService;
	/**
	 * Servicio para ejecutar transacciones con Disco la propiedad es inyectada
	 */
	@ManagedProperty(value = "#{DiscoService}")
	IDiscoService discoService;

	private Disco disco;
	private List<Alquiler> alquilerList;
	private SocioManagedBean socioMB;

	/**
	 * Este método se ejecuta seguido a la construcción del ManagedBean
	 */

	@PostConstruct
	public void init() {
		ExternalContext contexto = FacesContext.getCurrentInstance()
				.getExternalContext();
		this.setSocioMB((SocioManagedBean) contexto.getSessionMap().get(
				"socioMB"));
		this.setDisco(new Disco());
		this.loadAlquilerListBySocio();

	}

	public SocioManagedBean getSocioMB() {
		return socioMB;
	}

	public void setSocioMB(SocioManagedBean socioMB) {
		this.socioMB = socioMB;
	}

	public IAlquilerService getAlquilerService() {
		return alquilerService;
	}

	public void setAlquilerService(IAlquilerService alquilerService) {
		this.alquilerService = alquilerService;
	}

	public IDiscoService getDiscoService() {
		return discoService;
	}

	public void setDiscoService(IDiscoService discoService) {
		this.discoService = discoService;
	}

	public Disco getDisco() {
		return disco;
	}

	public void setDisco(Disco disco) {
		this.disco = disco;
	}

	public List<Alquiler> getAlquilerList() {
		return alquilerList;
	}

	public void setAlquilerList(List<Alquiler> alquilerList) {
		this.alquilerList = alquilerList;
	}

	/**
	 * 
	 * recupera la lista de alquileres segun usuario recuperado en
	 * SocioManagedBean
	 */
	public String loadAlquilerListBySocio() {
		this.setAlquilerList(new ArrayList<Alquiler>());
		try {
			this.getAlquilerList().addAll(
					this.getAlquilerService().getAlquilerListBySocioId(
							this.getSocioMB().getSocio().getId()));
			return SUCCESS;
		} catch (DataAccessException e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	/**
	 * Elimina un registro de alquiler y actuliza la lista de alquileres visible
	 * para el usuario
	 * 
	 * @return
	 */
	public String devolver() {
		Alquiler alquiler;
		try {
			alquiler = this.getAlquilerService()
					.getAlquilerByIdDiscoAndIdSocio(this.getDisco().getId(),
							this.getSocioMB().getSocio().getId());
			this.getAlquilerService().delete(alquiler);
			this.loadAlquilerListBySocio();
			this.getDisco().setId(0);
			// Despliega mensaje de notificación
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
					" Eliminado", null);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return SUCCESS;

		} catch (DataAccessException e) {
			e.printStackTrace();
			// Despliega mensaje de notificación
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, " Error eliminando", null);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return ERROR;
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			// Despliega mensaje de notificación
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					" Error eliminando este disco no esta alquilado", null);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return ERROR;
			// TODO: handle exception
		}
	}

	/**
	 * registra un alquiler para el usuario recuperado en SocioManagedBean
	 * 
	 * @return
	 */
	public String alquilar() {

		Alquiler alquiler = new Alquiler();
		Disco disco;
		try {
			disco = (Disco) getDiscoService().getById(this.getDisco().getId());
			alquiler.setDisco(disco);
			alquiler.setFechaPrestamo(Utileria.getFechaActual());
			alquiler.setSocio(socioMB.getSocio());
			this.getAlquilerService().add(alquiler);
			// this.loadAlquilerListBySocio();
			this.getAlquilerList().add(alquiler);
			this.getDisco().setId(0);

			// Despliega mensaje de notificación
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
					disco.getFilm().getTitulo() + " Alquilado", null);
			FacesContext.getCurrentInstance().addMessage(null, message);

			return SUCCESS;
		} catch (DataAccessException e) {
			e.printStackTrace();
			// Despliega mensaje de notificación
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, " Error alquilando", null);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return ERROR;
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					" Error este campo no puede contener un 0", null);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return ERROR;
			// TODO: handle exception
		}
	}
}