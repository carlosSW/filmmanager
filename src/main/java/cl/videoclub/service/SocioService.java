package cl.videoclub.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import cl.videoclub.model.Socio;
import cl.videoclub.dao.ISocioDAO;

/**
 * 
 * @author Carlos M. Toledo
 *
 */
@Transactional(readOnly = true)
public class SocioService implements ISocioService {

	ISocioDAO socioDAO;

	public ISocioDAO getSocioDAO() {
		return socioDAO;
	}

	public void setSocioDAO(ISocioDAO socioDAO) {
		this.socioDAO = socioDAO;
	}

	@Transactional(readOnly = false)
	public void add(Object socio) {
		getSocioDAO().add(socio);

	}

	@Transactional(readOnly = false)
	public void update(Object socio) {
		getSocioDAO().update(socio);

	}

	@Transactional(readOnly = false)
	public void delete(Object socio) {
		getSocioDAO().delete(socio);

	}

	public Socio getById(Object id) {
		return (Socio) getSocioDAO().getById(id);
	}

	public List<Socio> getAll() {
		return getSocioDAO().getAll();
	}

}
