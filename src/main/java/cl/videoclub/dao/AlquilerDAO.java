package cl.videoclub.dao;

import java.util.List;
import org.hibernate.SessionFactory;
import cl.videoclub.model.Alquiler;

/**
 * 
 * @author Carlos M. Toledo
 *
 */
public class AlquilerDAO implements IAlquilerDAO {

	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void add(Object alquiler) {
		getSessionFactory().getCurrentSession().save(alquiler);
	}

	public void update(Object alquiler) {
		getSessionFactory().getCurrentSession().update(alquiler);
	}

	public void delete(Object alquiler) {
		getSessionFactory().getCurrentSession().delete(alquiler);
	}

	public Object getById(Object id) {
		return (Alquiler) getSessionFactory().getCurrentSession()
				.createQuery("from Alquiler where id=?").setParameter(0, id)
				.list().get(0);
	}

	public List<Alquiler> getAll() {
		List alquiler = this.getSessionFactory().getCurrentSession()
				.createQuery("from Alquiler").list();
		return alquiler;
	}

	public List<Alquiler> getAlquilerListByIdSocio(int socioId) {
		List alquiler = this.getSessionFactory().getCurrentSession()
				.createQuery("from Alquiler where SOCIOid=?")
				.setParameter(0, socioId).list();
		return alquiler;
	}

	@Override
	public Alquiler getAlquilerByIdDisco(int idDisco) {
		// TODO Auto-generated method stub
		List<Alquiler> list = this.getSessionFactory().getCurrentSession()
				.createQuery("from Alquiler where DISCOid=?")
				.setParameter(0, idDisco).list();
		return (Alquiler)list.get(0);
	}

	@Override
	public Alquiler getAlquilerByIdDiscoAndIdSocio(int idDisco, int IdSocio) {
		// TODO Auto-generated method stub
		List<Alquiler> list = this.getSessionFactory().getCurrentSession()
				.createQuery("from Alquiler where DISCOid=? and SOCIOid=?")
				.setParameter(0, idDisco).setParameter(1, IdSocio).list();
		return (Alquiler)list.get(0);
	}
}
