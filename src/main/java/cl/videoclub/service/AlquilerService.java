package cl.videoclub.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import cl.videoclub.model.Alquiler;
import cl.videoclub.dao.IAlquilerDAO;
/**
 * 
 * @author Carlos M. Toledo
 *
 */
@Transactional(readOnly=true)
public class AlquilerService implements IAlquilerService{
	
	IAlquilerDAO alquilerDAO;

	public IAlquilerDAO getAlquilerDAO() {
		return alquilerDAO;
	}

	public void setAlquilerDAO(IAlquilerDAO alquilerDAO) {
		this.alquilerDAO = alquilerDAO;
	}

	@Transactional(readOnly=false)
	public void add(Object alquiler) {
		getAlquilerDAO().add(alquiler);
	}

	@Transactional(readOnly=false)
	public void update(Object alquiler) {
		getAlquilerDAO().update(alquiler);
	}

	@Transactional(readOnly=false)
	public void delete(Object alquiler) {
		getAlquilerDAO().delete(alquiler);
	}

	public Alquiler getById(Object id) {
		return (Alquiler) getAlquilerDAO().getById(id);
	}

	public List<Alquiler> getAll() {
		return getAlquilerDAO().getAll();
	}

	public List<Alquiler> getAlquilerListBySocioId(int socioId) {
		return getAlquilerDAO().getAlquilerListByIdSocio(socioId);
	}

	@Override
	public Alquiler getAlquilerListByDiscoId(int discoId) {
		// TODO Auto-generated method stub
		return this.getAlquilerDAO().getAlquilerByIdDisco(discoId);
	}

	@Override
	public Alquiler getAlquilerByIdDiscoAndIdSocio(int idDisco, int IdSocio) {
		// TODO Auto-generated method stub
		return this.getAlquilerDAO().getAlquilerByIdDiscoAndIdSocio(idDisco, IdSocio);
	}

	
}
