package cl.videoclub.dao;

import java.util.List;


import cl.videoclub.model.Alquiler;
/**
 * 
 * @author Carlos M. Toledo
 *
 */
public interface IAlquilerDAO extends IDAO{
	
	public List<Alquiler> getAlquilerListByIdSocio(int socioId);
	public Alquiler getAlquilerByIdDisco(int idDisco);
	public Alquiler getAlquilerByIdDiscoAndIdSocio(int idDisco, int IdSocio);
}
