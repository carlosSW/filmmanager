-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: jd2DB
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ALQUILER`
--

DROP TABLE IF EXISTS `ALQUILER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ALQUILER` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fecha_prestamo` char(10) NOT NULL,
  `DISCOid` int(10) NOT NULL,
  `SOCIOid` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `DISCOid` (`DISCOid`),
  KEY `FKALQUILER492368` (`DISCOid`),
  KEY `FKALQUILER339981` (`SOCIOid`),
  CONSTRAINT `FKALQUILER339981` FOREIGN KEY (`SOCIOid`) REFERENCES `SOCIO` (`id`),
  CONSTRAINT `FKALQUILER492368` FOREIGN KEY (`DISCOid`) REFERENCES `DISCO` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ALQUILER`
--

LOCK TABLES `ALQUILER` WRITE;
/*!40000 ALTER TABLE `ALQUILER` DISABLE KEYS */;
/*!40000 ALTER TABLE `ALQUILER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DISCO`
--

DROP TABLE IF EXISTS `DISCO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DISCO` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `FILMid` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKDISCO18613` (`FILMid`),
  CONSTRAINT `FKDISCO18613` FOREIGN KEY (`FILMid`) REFERENCES `FILM` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DISCO`
--

LOCK TABLES `DISCO` WRITE;
/*!40000 ALTER TABLE `DISCO` DISABLE KEYS */;
INSERT INTO `DISCO` VALUES (1,1),(7,1),(13,1),(2,2),(8,2),(14,2),(3,3),(9,3),(15,3),(4,4),(10,4),(16,4),(5,5),(11,5),(17,5),(6,6),(12,6),(18,6);
/*!40000 ALTER TABLE `DISCO` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FILM`
--

DROP TABLE IF EXISTS `FILM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FILM` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(55) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `titulo` (`titulo`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FILM`
--

LOCK TABLES `FILM` WRITE;
/*!40000 ALTER TABLE `FILM` DISABLE KEYS */;
INSERT INTO `FILM` VALUES (1,'Iron Man 1'),(2,'Iron Man 2'),(3,'Iron Man 3'),(4,'The Avengers'),(5,'Thor'),(6,'Thor 2');
/*!40000 ALTER TABLE `FILM` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SOCIO`
--

DROP TABLE IF EXISTS `SOCIO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SOCIO` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(55) NOT NULL,
  `dni` varchar(55) NOT NULL,
  `apellido` varchar(55) NOT NULL,
  `fecha_nac` char(10) NOT NULL,
  `sexo` char(1) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `fono` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dni` (`dni`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SOCIO`
--

LOCK TABLES `SOCIO` WRITE;
/*!40000 ALTER TABLE `SOCIO` DISABLE KEYS */;
INSERT INTO `SOCIO` VALUES (1,'Carlos','17427423-1','Toledo','24-03-1990','M','En mi casa 1454','+5692322424'),(2,'Fabricio','16653952-9','Millaguir','24-05-1989','M','En mi casa 1456','+5692322424'),(3,'yvguhhjkb','768790','hbjjb','jhhbjb','n','uhnjmkl','hjkkjjk'),(4,'Raphael','5467890','Verdugo','32/2/1980','M','omookm','567890');
/*!40000 ALTER TABLE `SOCIO` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-23  7:06:29
