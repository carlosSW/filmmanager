package cl.videoclub.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * @author Carlos M. Toledo
 *
 */
@Entity
@org.hibernate.annotations.Proxy(lazy = false)
@Table(name = "DISCO")
public class Disco {
	private int id;
	private Film film;
	private Alquiler alquiler;

	@Id
	@Column(name = "ID", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(targetEntity = cl.videoclub.model.Film.class, fetch = FetchType.LAZY)
	@org.hibernate.annotations.Cascade({ org.hibernate.annotations.CascadeType.LOCK })
	@JoinColumns({ @JoinColumn(name = "FILMid", referencedColumnName = "id", nullable = false) })
	@org.hibernate.annotations.LazyToOne(value = org.hibernate.annotations.LazyToOneOption.NO_PROXY)
	public Film getFilm() {
		return film;
	}

	public void setFilm(Film film) {
		this.film = film;
	}

	@OneToOne(mappedBy = "disco")
	public Alquiler getAlquiler() {
		return alquiler;
	}

	public void setAlquiler(Alquiler alquiler) {
		this.alquiler = alquiler;
	}

	@Override
	public String toString() {
		return "Disco [id=" + id + ", film=" + film + ", alquiler=" + alquiler
				+ "]";
	}

}
