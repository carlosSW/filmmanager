package cl.videoclub.dao;

import java.util.List;

import org.hibernate.SessionFactory;

import cl.videoclub.model.Disco;

/**
 * 
 * @author Carlos M. Toledo
 *
 */
public class DiscoDAO implements IDiscoDAO{
	
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void add(Object disco) {
		getSessionFactory().getCurrentSession().save(disco);
	}

	public void update(Object disco) {
		getSessionFactory().getCurrentSession().update(disco);
	}

	public void delete(Object disco) {
		getSessionFactory().getCurrentSession().delete(disco);
	}

	public Object getById(Object id) {
		return (Disco)getSessionFactory().getCurrentSession()
				.createQuery("from Disco where id=?").setParameter(0, id)
				.list().get(0);
	}

	public List<Disco> getAll() {
        List discos = this.getSessionFactory().getCurrentSession()
                .createQuery("from Disco").list();
        return discos;
	}
	
	
}
