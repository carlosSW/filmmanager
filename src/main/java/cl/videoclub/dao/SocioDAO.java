package cl.videoclub.dao;

import java.util.List;
import org.hibernate.SessionFactory;
import cl.videoclub.model.Socio;

/**
 * 
 * @author Carlos M. Toledo
 *
 */
public class SocioDAO implements ISocioDAO{
	
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void add(Object socio) {
		getSessionFactory().getCurrentSession().save(socio);
	}

	public void update(Object socio) {
		getSessionFactory().getCurrentSession().update(socio);
	}

	public void delete(Object socio) {
		getSessionFactory().getCurrentSession().delete(socio);
	}

	public Object getById(Object id) {
		return (Socio)getSessionFactory().getCurrentSession()
				.createQuery("from Socio where id=?").setParameter(0, id)
				.list().get(0);
	}

	public List<Socio> getAll() {
		List socios=this.getSessionFactory().getCurrentSession().createQuery("from Socio").list();
		return socios; 
	}
	
	
}
