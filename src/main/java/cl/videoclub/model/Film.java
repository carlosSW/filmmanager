package cl.videoclub.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author Carlos M. Toledo
 *
 */
@Entity
@org.hibernate.annotations.Proxy(lazy = false)
@Table(name = "FILM")
public class Film {
	private int id;
	private String titulo;
	private Set<Disco> discos;

	@Id
	@Column(name = "ID", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "TITULO", unique = false, nullable = false)
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	@OneToMany(mappedBy = "film")
	public Set<Disco> getDiscos() {
		return discos;
	}

	public void setDiscos(Set<Disco> discos) {
		this.discos = discos;
	}

	@Override
	public String toString() {
		return "Film [id=" + id + ", titulo=" + titulo + "]";
	}

}
