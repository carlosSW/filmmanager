package cl.videoclub.dao;

import java.util.List;

import org.hibernate.SessionFactory;

import cl.videoclub.model.Film;
/**
 * 
 * @author Carlos M. Toledo
 *
 */
public class FilmDAO implements IFilmDAO{
	
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void add(Object film) {
		getSessionFactory().getCurrentSession().save(film);
	}

	public void update(Object film) {
		getSessionFactory().getCurrentSession().update(film);
	}

	public void delete(Object film) {
		getSessionFactory().getCurrentSession().delete(film);
	}

	public Object getById(Object id) {
		return (Film)getSessionFactory().getCurrentSession()
				.createQuery("from Film where id=?").setParameter(0, id)
				.list().get(0);
	}

	public List<Film> getAll() {
		List film = this.getSessionFactory().getCurrentSession().createQuery("from Film").list();
		return film;
	}
	
	
}
