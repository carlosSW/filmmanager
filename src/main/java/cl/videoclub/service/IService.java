package cl.videoclub.service;

import java.util.List;

/**
 *
 * @author fabricio
 */
public interface IService {
    
    public void add(Object object);
    
    public void update(Object object);
    
    public void delete(Object object);
    
    public Object getById(Object id);
    
    public List getAll();
    
    
}
