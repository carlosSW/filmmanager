package cl.videoclub.service;

import java.util.List;
/**
 * 
 * @author Carlos M. Toledo
 *
 */

import org.springframework.transaction.annotation.Transactional;

import cl.videoclub.model.Film;
import cl.videoclub.dao.IFilmDAO;
@Transactional(readOnly=true)
public class FilmService implements IFilmService{
	
	IFilmDAO filmDAO;

	public IFilmDAO getFilmDAO() {
		return filmDAO;
	}

	public void setFilmDAO(IFilmDAO filmDAO) {
		this.filmDAO = filmDAO;
	}
	
	@Transactional(readOnly=false)
	public void add(Object film) {
		getFilmDAO().add(film);
	}

	@Transactional(readOnly=false)
	public void update(Object film) {
		getFilmDAO().update(film);
		
	}

	@Transactional(readOnly=false)
	public void delete(Object film) {
		getFilmDAO().delete(film);
		
	}

	public Film getById(Object id) {
		return (Film) getFilmDAO().getById(id);
	}

	public List<Film> getAll() {
		return getFilmDAO().getAll();
	}
	
	
}
