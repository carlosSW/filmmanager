package cl.videoclub.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author Carlos M. Toledo
 *
 */
@Entity
@org.hibernate.annotations.Proxy(lazy = false)
@Table(name = "SOCIO")
public class Socio {
	private int id;
	private String nombre;
	private Set<Alquiler> alquiler;
	private String dni;
	private String apellido;
	private String fechaNacimiento;
	private char sexo;
	private String direccion;
	private String fono;

	@Id
	@Column(name = "ID", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "NOMBRE", unique = true, nullable = false)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@OneToMany(mappedBy = "socio")
	public Set<Alquiler> getAlquiler() {
		return alquiler;
	}

	public void setAlquiler(Set<Alquiler> alquiler) {
		this.alquiler = alquiler;
	}

	@Column(name = "DNI", unique = true, nullable = false)
	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	@Column(name = "APELLIDO", unique = false, nullable = false)
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@Column(name = "FECHA_NAC")
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@Column(name = "SEXO", unique = false, nullable = false)
	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	@Column(name = "DIRECCION", unique = false, nullable = false)
	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@Column(name = "FONO", unique = false, nullable = false)
	public String getFono() {
		return fono;
	}

	public void setFono(String fono) {
		this.fono = fono;
	}

	@Override
	public String toString() {
		return "Socio [id=" + id + ", nombre=" + nombre + ", alquiler="
				+ alquiler + ", dni=" + dni + ", apellido=" + apellido
				+ ", fechaNacimiento=" + fechaNacimiento + ", sexo=" + sexo
				+ ", direccion=" + direccion + ", fono=" + fono + "]";
	}
}
