package cl.videoclub.managed.bean;

import cl.videoclub.model.Socio;
import cl.videoclub.service.ISocioService;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.PostUpdate;

import org.springframework.dao.DataAccessException;

/**
 *
 * @author Carlos M. Toledo
 */
@ManagedBean(name = "socioMB")
@SessionScoped
public class SocioManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String SUCCESS = "success";
	private static final String ERROR = "error";
	
	/**
	 * Servicio para ejecutar transacciones con Socio, la propiedad es inyectada
	 */
	@ManagedProperty(value = "#{SocioService}")
	ISocioService socioService;
	/**
	 * este metodo se ejecuta inmediatamente despues de la construcción del ManagedBean
	 */
	@PostConstruct
	public void init() {
		this.setSocio(new Socio());	
	}
	private Socio socio;
	
	public ISocioService getSocioService() {
		return socioService;
	}

	public void setSocioService(ISocioService socioService) {
		this.socioService = socioService;
	}

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socio) {
		this.socio = socio;
	}
	/**
	 * recupera un socio 
	 */
	public String loadSocio() {
		try {
			this.setSocio((Socio) this.getSocioService().getById(
					this.getSocio().getId()));
			return SUCCESS;
		} catch (DataAccessException e) {
			e.printStackTrace();
			return ERROR;
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			////Despliega mensaje de notificación
			FacesMessage message = new FacesMessage(
					FacesMessage.SEVERITY_ERROR, " Error usuario no existe", null);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return ERROR;
			// TODO: handle exception
		}

	}
	/**
	 * registra un nuevo socio
	 * @return
	 */
	public String addSocio() {
		try {
			this.getSocioService().add(this.getSocio());
			return SUCCESS;
		} catch (DataAccessException e) {
			e.printStackTrace();
			return ERROR;
		}
	}

}